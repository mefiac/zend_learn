<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180527034536 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('document');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('title', 'text', ['notnull'=>true]);
        $table->addColumn('tho_gave', 'text', ['notnull'=>true]);
        $table->addColumn('number', 'integer', ['notnull'=>true]);
        $table->addColumn('date_gave', 'datetime', ['notnull'=>true]);
        $table->addColumn('id_user', 'integer', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
