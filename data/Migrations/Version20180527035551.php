<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180527035551 extends AbstractMigration
{
    public function up(Schema $schema): void
    {

        $table = $schema->getTable('document');
        $table->addIndex(['id_user'], 'user_id_index');
        $table->addForeignKeyConstraint('user', ['id_user'], ['id'], [], 'user_id_fk');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
