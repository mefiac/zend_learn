<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered user.
 * @ORM\Entity(repositoryClass="\Application\Repository\DocumentRepository")
 * @ORM\Table(name="document")
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="title")
     */
    protected $title;

    /**
     * @ORM\Column(name="tho_gave")
     */
    protected $tho_gave;

    /**
     * @ORM\Column(name="number")
     */
    protected $number;

    /**
     * @ORM\Column(name="date_gave")
     */
    protected $date_gave;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     */
    protected $id_user;


    public function getId()
    {
        return $this->id;
    }


    /**
     * Sets ID of this post.
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns user ID.
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Returns user date_gave.
     * @return
     */
    public function getDate_gave()
    {
        return $this->date_gave;
    }

    /**
     * Returns user tho_gave.
     * @return
     */
    public function getTho_gave()
    {
        return $this->tho_gave;
    }

    /**
     * Sets $number.
     * @param $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * Sets date_gave.
     * @param date_gave
     */
    public function setDate_gave($date_gave)
    {
        $this->date_gave = $date_gave;
    }

    /**
     * Sets user tho_gave.
     * @param int tho_gave
     */
    public function setTho_gave($tho_gave)
    {
        $this->tho_gave = $tho_gave;
    }


    /**
     * Returns title.
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns id_user.
     * @return string
     */
    public function getId_user()
    {
        return $this->id_user;
    }

    /**
     * set id_user.
     * @return string
     */
    public function setId_user($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * Sets title.
     * @param string title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


}