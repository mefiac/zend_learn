<?php

namespace Application\Repository;

use Application\Entity\Document;
use Doctrine\ORM\EntityRepository;

/**
 * This is the custom repository class for Post entity.
 */
class DocumentRepository extends EntityRepository
{

    public function findAndSortDocuments($row,$filter)
    {
        $row=(!empty($row)) ? $row : 'id';
        $entityManager = $this->getEntityManager();
        $row = 'd.' . $row;
        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder->select('d')
            ->from(Document::class, 'd')
            ->orderBy($row, $filter);
        return $queryBuilder->getQuery();
    }



}