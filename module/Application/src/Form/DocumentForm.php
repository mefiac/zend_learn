<?php

namespace Application\Form;

use Application\Filter\PhoneFilter;
use Application\Validator\PhoneValidator;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class DocumentForm extends Form
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('document-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();

    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {

        // Add "title" field
        $this->add([
            'type' => 'text',
            'name' => 'title',
            'attributes' => [
                'id' => 'title'
            ],
            'options' => [
                'label' => 'Document Title',
            ],
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'tho_gave',
            'attributes' => [
                'id' => 'tho_gave'
            ],
            'options' => [
                'label' => 'Who give document',
                //'pattern'=>'\w[ ]\w{2,}\w'
            ],
        ]);


        $this->add([
            'type' => 'date',
            'name' => 'date_gave',
            'attributes' => [
                'id' => 'date_gave'
            ],
            'options' => [
                'label' => 'Content',
                'format'=>'d.m.Y'
            ],
        ]);
        $this->add([
            'type' => 'number',
            'name' => 'number',
            'attributes' => [
                'id' => 'number'
            ],
            'options' => [
                'label' => 'Content',
            ],
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }


    private function addInputFilter()
    {

        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
                ['name' => 'StripNewlines'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 7
                    ],
                ],
            ],
        ]);
        $inputFilter->add([
            'name' => 'number',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
                ['name' => 'StripNewlines'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 3,
                        'max' => 300,
                        'pattern' => '^[ 0-9]+$'
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'tho_gave',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 3,
                        'max' => 4000,
                    ],
                ],
                [
                    'name' => 'Regex',
                    'options' => [
                        'pattern' => '/([a-zA-Zа-яА-Я])\w[ ]\w+/'

                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'date_gave',
            'required' => true,
            'filters' => [
                ['name' => 'StripNewlines'],
            ],
            'validators' => [
                [
                    'name' => 'Date',
                    'options' => [
                        'format'=>'d.m.Y'
                    ],
                ],
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => [$this, 'validateDate'],
                        'callbackOptions' => [
                            'format' => 'intl'
                        ]
                    ]
                ]

            ],
        ]);

    }
    public function validateDate($value)
    {

        $result=(strtotime($value.'+1 days')<strtotime(date('d.m.Y')));

        return $result;
    }
}

