<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\IndexController;
use User\Service\AuthAdapter;
use User\Service\AuthManager;
use User\Service\UserManager;
/**
 * This is the factory for IndexController. Its purpose is to instantiate the
 * controller and inject dependencies into it.
 */
class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
     //   $postManager = $container->get(PostManager::class);
        // Instantiate the controller and inject dependencies
        return new IndexController($entityManager);
    }
}