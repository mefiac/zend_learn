<?php

namespace Application\Controller;

use Application\Entity\Document;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;


/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class IndexController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function indexAction()
    {
        $page = $this->params()->fromQuery('page', 1);

        $filter = $this->params()->fromQuery('filter', 'ASC');

        $row = $this->params()->fromQuery('row', 'id');

        $filter = ($filter == 'ASC') ? "DESC" : "ASC";

        $query = $this->entityManager->getRepository(Document::class)
            ->findAndSortDocuments($row, $filter);

        if ($query == null) {
            return new ViewModel([
                'start' => true
            ]);
        }

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));

        $paginator = new Paginator($adapter);

        $paginator->setDefaultItemCountPerPage(2);

        $paginator->setCurrentPageNumber($page);

        return new ViewModel([
            'documents' => $paginator,
            'filter' => $filter,
            'page' => $page
        ]);
    }


    /**
     * This is the "about" action. It is used to display the "About" page.
     */
    public function aboutAction()
    {
        $appName = 'User Demo';
        $appDescription = 'This demo shows how to implement user management with Zend Framework 3';

        // Return variables to view script with the help of
        // ViewObject variable container
        return new ViewModel([
            'appName' => $appName,
            'appDescription' => $appDescription
        ]);
    }


}

