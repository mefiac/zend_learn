<?php

namespace Application\Controller;

use Application\Entity\Document;
use Application\Form\DocumentForm;
use User\Entity\User;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class DocumentController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;

    /**
     * Post manager.
     * @var Application\Service\DocumentManager
     */
    private $documentManager;

    public function __construct($entityManager, $authService, $documentManager)
    {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
        $this->documentManager = $documentManager;
    }

    public function deleteAction()
    {
        $postId = (int)$this->params()->fromRoute('id', -1);


        if ($postId < 0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $document = $this->entityManager->getRepository(Document::class)
            ->findOneById($postId);
        if ($document == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $is_admin = ($this->authService->getIdentity() == $document->getId_user()->getEmail()) ? true : false;
        if ($is_admin) {
            $this->documentManager->removeDocument($document);
        } else {
            $this->getResponse()->setStatusCode(403);
            return;
        }


        return $this->redirect()->toRoute('home');
    }

    public function viewAction()
    {


        $form = new DocumentForm();


        $postId = (int)$this->params()->fromRoute('id', -1);


        if ($postId < 0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }


        $document = $this->entityManager->getRepository(Document::class)
            ->findOneById($postId);


        if ($document == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $id = $document->getId();
        $is_admin = ($this->authService->getIdentity() == $document->getId_user()->getEmail()) ? true : false;

        if ($this->getRequest()->isPost() && $is_admin) {


            $data = $this->params()->fromPost();


            $form->setData($data);
            if ($form->isValid()) {

                $data = $form->getData();

                $this->documentManager->updateDocument($document, $data);

            }
        } else {

            $date_gave = date('d.m.Y', strtotime($document->getDate_gave()));
            $data = [
                'title' => $document->getTitle(),
                'tho_gave' => $document->getTho_gave(),
                'number' => $document->getNumber(),
                'date_gave' => $date_gave
            ];

            $form->setData($data);
        }

        return new ViewModel([
            'form' => $form,
            'admin' => $is_admin,
            'id' => $id

        ]);
    }

    public function createAction()
    {
        $user = $this->entityManager->getRepository(User::class)
            ->findOneByEmail($this->authService->getIdentity());
        $form = new DocumentForm();

        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {

                $data = $form->getData();

                $this->documentManager->addNewDocument($data, $user);

                return $this->redirect()->toRoute('home');
            }
        }

        return new ViewModel([
            'form' => $form


        ]);
    }

}
