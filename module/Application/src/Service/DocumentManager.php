<?php

namespace Application\Service;

use Application\Entity\Document;

class DocumentManager
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager;
     */
    private $entityManager;

    /**
     * Constructor.
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updateDocument($post, $data)
    {
        $post->setTitle($data['title']);
        $post->setNumber($data['number']);
        $post->setTho_gave($data['tho_gave']);
        $post->setDate_gave(date('Y-m-d H:i:s', strtotime($data['date_gave'])));
        $this->entityManager->flush();
    }

    public function removeDocument($post)
    {
        $this->entityManager->remove($post);
        $this->entityManager->flush();
    }

    public function addNewDocument($data, $user)
    {
        $post = new Document();
        $post->setTitle($data['title']);
        $post->setNumber($data['number']);
        $post->setTho_gave($data['tho_gave']);
        $post->setDate_gave($data['date_gave']);
        $post->setId_user($user);
        $this->entityManager->persist($post);
        $this->entityManager->flush();
    }


}
